package com.builders.apibuilders.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.handler.RequestParameter;
import com.builders.apibuilders.models.dto.ClienteDto;
import com.builders.apibuilders.services.ClienteService;

@DisplayName("Cliente Controller Tests")
//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ClienteControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private ClienteService clienteService;
	
	@Mock
	private Pageable pageable;
	
	private ClienteDto clienteDto;
	
	private Cliente cliente;
	
	@LocalServerPort
    private int port;
	
	private final int TIMEOUT = 3000; 
	
	private String getRootUrl() {
        return "http://localhost:" + port;
    }
	
	private RestTemplate getRestTemplate() {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(TIMEOUT);
		requestFactory.setReadTimeout(TIMEOUT);
		restTemplate.setRequestFactory(requestFactory);
		return restTemplate;
		
	}
	
	@Before
	public void setup() {
	    restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	}
	
	@BeforeEach
	private void beforeEachSetup() {
		
		clienteDto = ClienteDto.builder()
				 	.setId(1)
					.setCpf("12345678901")
					.setNome("cliente01")
					.setDataNascimento(new Date())
					.build();
		
		cliente = Cliente.builder()
				.setId(1)
				.setCpf("12345678901")
				.setNome("cliente01")
				.setDataNascimento(new Date())
				.build();
	}
	
	@DisplayName("Seach Cliente When Fill All Query Parameter Should Return Status Code 200")
	@Test
	void searchCliente_WhenFillAllQueryParameter_ShouldReturnStatusCode200() throws Exception{
		
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(this.cliente);
        Page<Cliente> pageable = new PageImpl<>(clientes);
		
        Mockito.when(clienteService.findPage(ArgumentMatchers.any(RequestParameter.class))).thenReturn(pageable);
		
		String url =  getRootUrl() + "/clientes?page=1&linesPerPage=10&orderBy=nome&direction=ASC";
		
		ResponseEntity<String> response  = this.restTemplate.getForEntity(url,String.class);
		Assertions.assertEquals(HttpStatus.OK.value() ,response.getStatusCodeValue());
		
	}
	
	@DisplayName("Seach Cliente When Fill All Query Parameter Should Return Content Not Empty")
	@Test
	void searchCliente_WhenFillAllQueryParameter_ShouldReturnPageableContentNotEmpty() throws Exception{
		
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(this.cliente);
        Page<Cliente> pageable = new PageImpl<>(clientes);
		
		Mockito.when(clienteService.findPage(ArgumentMatchers.any(RequestParameter.class))).thenReturn(pageable);
		
		String url =  getRootUrl() + "/clientes?page=1&linesPerPage=10&orderBy=nome&direction=ASC";
		
		ResponseEntity<String> response  = this.restTemplate.getForEntity(url,String.class);
		
		Assertions.assertEquals(false, response.getBody().isEmpty());
		
	}
	
	@DisplayName("Seach Cliente When Not Fill All Query Parameter Should Return Content Not Empty")
	@Test
	void searchCliente_WhenNotFillAllQueryParameter_ShouldReturnPageableContentNotEmpty() throws Exception{
		
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(this.cliente);
        Page<Cliente> pageable = new PageImpl<>(clientes);
		
		Mockito.when(clienteService.findPage(ArgumentMatchers.any(RequestParameter.class))).thenReturn(pageable);
		
		String url =  getRootUrl() + "/clientes";
		
		ResponseEntity<String> response  = this.restTemplate.getForEntity(url,String.class);
		
		Assertions.assertEquals(false, response.getBody().isEmpty());
		
	}
	
	@DisplayName("Create Cliente When Not Fill All Fields Should Return Status Code 400")
	@Test
	void createCliente_WhenNotFillAllFields_ShouldReturnStatusCode400() throws Exception{
		
		ClienteDto clienteDto = ClienteDto.builder().build();
		
		String url =  getRootUrl() + "/clientes";
		ResponseEntity<String> response  = this.restTemplate.postForEntity(url,clienteDto,String.class);
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value() ,response.getStatusCodeValue());
		
	}
	
	@DisplayName("Create Cliente When Fill All Fields Should Return Status Code 201")
	@Test
	void createCliente_WhenFillAllFields_ShouldReturnStatusCode201() throws Exception{
		
		Mockito.when(clienteService.create(ArgumentMatchers.any(Cliente.class))).thenReturn(cliente);
		
		String url =  getRootUrl() + "/clientes";
		String location = url + "/" + cliente.getId();
		ResponseEntity<String> response  = this.restTemplate.postForEntity(url,clienteDto,String.class);
		Assertions.assertEquals(HttpStatus.CREATED.value() ,response.getStatusCodeValue());
		
	}
	
	@DisplayName("Create Cliente When Fill All Fields Should Return Location")
	@Test
	void createCliente_WhenFillAllFields_ShouldReturnLocation() throws Exception{
		
		Mockito.when(clienteService.create(ArgumentMatchers.any(Cliente.class))).thenReturn(cliente);
		
		String url =  getRootUrl() + "/clientes";
		String location = url + "/" + cliente.getId();
		ResponseEntity<String> response  = this.restTemplate.postForEntity(url,clienteDto,String.class);
		Assertions.assertEquals(location,response.getHeaders().getLocation().toString());
		
	}
	
	@DisplayName("Delete Cliente When Sends Id Return Status Code 204")
	@Test
	void deleteCliente_WhenSendsId_ReturnStatusCode204() {
		
		Mockito.when(clienteService.findById(clienteDto.getId())).thenReturn(cliente);
		
		String url =  getRootUrl() + "/clientes/" + clienteDto.getId();
		ResponseEntity<String> response  = this.restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
		Assertions.assertEquals(HttpStatus.NO_CONTENT.value() ,response.getStatusCodeValue());
		Assertions.assertEquals(false ,response.hasBody());
	}

	@DisplayName("Update Cliente When Not Sends All Fields Return Status Code 400")
	@Test
	void updateCliente_WhenNotSendsAllFields_ShouldReturnStatusCode400() {
		
		ClienteDto clienteDto = ClienteDto.builder().build();
		HttpEntity<ClienteDto> clienteDtoHe = new HttpEntity<ClienteDto>(clienteDto);
		
		String url =  getRootUrl() + "/clientes/{id}";
		ResponseEntity<String> response  = this.restTemplate.exchange(url, HttpMethod.PUT, clienteDtoHe, String.class,this.clienteDto.getId());
		
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value() ,response.getStatusCodeValue());
	}
	
	@DisplayName("Update Cliente When Sends All Fields Return Status Code 204")
	@Test
	void updateCliente_WhenSendsAllFields_ShouldReturnStatusCode204() {
		
		Mockito.when(clienteService.findById(clienteDto.getId())).thenReturn(cliente);
		
		HttpEntity<ClienteDto> clienteDtoHe = new HttpEntity<ClienteDto>(clienteDto);
		
		String url =  getRootUrl() + "/clientes/{id}";
		ResponseEntity<String> response  = this.restTemplate.exchange(url, HttpMethod.PUT, clienteDtoHe, String.class,clienteDto.getId());
		
		Assertions.assertEquals(HttpStatus.NO_CONTENT.value() ,response.getStatusCodeValue());
		Assertions.assertEquals(false ,response.hasBody());
	}
	
	@DisplayName("Patch Cliente When Not Sends All Fields Return Status Code 204")
	@Test
	void patchCliente_WhenNotSendsAllFields_ShouldReturnStatusCode204() {
		
		ClienteDto clienteDto = ClienteDto.builder().build();
		HttpEntity<ClienteDto> clienteDtoHe = new HttpEntity<ClienteDto>(clienteDto);
		
		String url =  getRootUrl() + "/clientes/{id}";
		ResponseEntity<String> response  = this.getRestTemplate().exchange(url, HttpMethod.PATCH, clienteDtoHe, String.class,this.clienteDto.getId());
		
		Assertions.assertEquals(HttpStatus.NO_CONTENT.value() ,response.getStatusCodeValue());
	}
	
	@DisplayName("Patch Cliente When Sends All Fields Return Status Code 204")
	@Test
	void patchCliente_WhenSendsAllFields_ShouldReturnStatusCode204() {
		
		Mockito.when(clienteService.findById(clienteDto.getId())).thenReturn(cliente);
		
		HttpEntity<ClienteDto> clienteDtoHe = new HttpEntity<ClienteDto>(clienteDto);
		
		String url =  getRootUrl() + "/clientes/{id}";
		ResponseEntity<String> response  = this.getRestTemplate().exchange(url, HttpMethod.PATCH, clienteDtoHe, String.class,this.clienteDto.getId());
		
		Assertions.assertEquals(HttpStatus.NO_CONTENT.value() ,response.getStatusCodeValue());
		Assertions.assertEquals(false ,response.hasBody());
	}
	
	
	
}
