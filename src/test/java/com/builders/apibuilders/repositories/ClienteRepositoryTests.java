package com.builders.apibuilders.repositories;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;

import com.builders.apibuilders.domains.Cliente;

@DisplayName("Cliente Repository Tests")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
public class ClienteRepositoryTests {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	private Cliente cliente;
	
	@BeforeAll
	void setup() {
	}

	@BeforeEach
	void init() {
		this.cliente = Cliente.builder()
				.setNome("Cliente01")
				.setCpf("12345678901")
				.setDataNascimento(new Date())
				.build();
	}
	
	@DisplayName("Create Shoud Persiste Data")
	@Test
	@Transactional
	@Rollback(true)
	void createShoudPersisteData() {
		
		Cliente clienteCreated = this.clienteRepository.save(this.cliente);
		Assertions.assertNotEquals(null, cliente.getId());
		Assertions.assertEquals(this.cliente.getNome(), clienteCreated.getNome());
		Assertions.assertEquals(this.cliente.getCpf(), clienteCreated.getCpf());
		Assertions.assertEquals(this.cliente.getDataNascimento(), clienteCreated.getDataNascimento());

	}

	@DisplayName("Delete Shoud Remove Data")
	@Test
	@Transactional
	@Rollback(true)
	void deleteShoudRemoveData() {

		Cliente clienteCreated = this.clienteRepository.save(this.cliente);
		this.clienteRepository.delete(clienteCreated);
		Optional<Cliente> clienteOpt = this.clienteRepository.findById(clienteCreated.getId());
		Assertions.assertEquals(Optional.empty(), clienteOpt);

	}

	@DisplayName("Update Shoud Change and Persist Data")
	@Test
	@Transactional
	@Rollback(true)
	void updateShoudChangeAndPersistData() throws ParseException {
		
		DateFormat f = DateFormat.getDateInstance();
		Date dataNascimento = f.parse("12/01/2000");
		
		Cliente cliente = this.clienteRepository.save(this.cliente);
		cliente.setNome("Cliente03");
		cliente.setCpf("09876543210");
		cliente.setDataNascimento(dataNascimento);
		cliente = this.clienteRepository.save(cliente);
		Assertions.assertNotEquals(null, cliente);
		Assertions.assertEquals("Cliente03", cliente.getNome());
		Assertions.assertEquals("09876543210", cliente.getCpf());
		Assertions.assertEquals(dataNascimento, cliente.getDataNascimento());

	}

	@DisplayName("Create When Field Is Null Should Throw Constraint Violation Exception")
	@Test
	void createWhenFieldIsNullShouldThrowConstraintViolationException() {

		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			Cliente cliente = Cliente.builder().build();
			cliente = this.clienteRepository.save(cliente);
		});

	}
	

}
