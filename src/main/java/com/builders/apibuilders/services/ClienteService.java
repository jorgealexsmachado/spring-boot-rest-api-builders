package com.builders.apibuilders.services;

import com.builders.apibuilders.domains.Cliente;

public interface ClienteService extends Service<Cliente> {
	
}
