package com.builders.apibuilders.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.handler.RequestParameter;
import com.builders.apibuilders.repositories.ClienteRepository;
import com.builders.apibuilders.services.exceptions.DataIntegrityException;
import com.builders.apibuilders.services.exceptions.ObjectNotFoundException;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente create(Cliente cliente) {
		cliente.setId(null);
		cliente = clienteRepository.save(cliente);
		return cliente;
	}
	
	public Page<Cliente> findPage(RequestParameter<Cliente> requestParameter) {
		
		return clienteRepository.search(requestParameter.getQuery().getCpf(), requestParameter.getPageRequest());
		
	}

	public Cliente findById(Integer id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return cliente.orElseThrow(()-> new ObjectNotFoundException("resource not found"));
	}

	public Cliente update(Cliente cliente) {
		Cliente newCliente = findById(cliente.getId());
		newCliente.setNome(cliente.getNome());
		newCliente.setCpf(cliente.getCpf());
		newCliente.setDataNascimento(cliente.getDataNascimento());
		return clienteRepository.save(newCliente);
	}
	
	public Cliente patch(Cliente cliente) {
		Cliente newCliente = findById(cliente.getId());
		
		if(cliente.getNome() != null)
			newCliente.setNome(cliente.getNome());
		
		if(cliente.getCpf() != null)
			newCliente.setCpf(cliente.getCpf());
		
		if(cliente.getDataNascimento() != null)
			newCliente.setDataNascimento(cliente.getDataNascimento());
		
		return clienteRepository.save(newCliente);
	}

	public void delete(int id) {
		findById(id);
		
		try {
			clienteRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Nao foi possivel deletar este cliente");
		}
		
	}
	
}
