package com.builders.apibuilders.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.handler.RequestParameter;

public interface Service<T> {

	public T create(T t);
	
	public Page<T> findPage(RequestParameter<T> requestParameter);
	
	public T findById(Integer id);
	
	public T update(T t);
	
	public T patch(T t);
	
	public void delete(int id);
	
}