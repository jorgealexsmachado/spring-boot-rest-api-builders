package com.builders.apibuilders.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.builders.apibuilders.domains.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	@Query("FROM Cliente c WHERE (:cpf is null or c.cpf = :cpf)")
	Page<Cliente> search(@Param("cpf") String cpf, Pageable pageable);

}
