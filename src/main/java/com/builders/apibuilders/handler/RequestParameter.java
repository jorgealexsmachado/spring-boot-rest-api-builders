package com.builders.apibuilders.handler;

import org.springframework.data.domain.PageRequest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class RequestParameter <T>{

	private PageRequest pageRequest;
	private T query;
}
