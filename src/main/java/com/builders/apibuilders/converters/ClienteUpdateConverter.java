package com.builders.apibuilders.converters;

import org.springframework.stereotype.Component;

import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.models.dto.ClienteUpdateDto;

@Component
public class ClienteUpdateConverter extends Converter<ClienteUpdateDto, Cliente> {

	  public ClienteUpdateConverter() {
	    super(ClienteUpdateConverter::toEntity, ClienteUpdateConverter::toDto);
	  }
	  
	  private static ClienteUpdateDto toDto(Cliente cliente) {
	    return ClienteUpdateDto.builder()
	    				.setId(cliente.getId())
	    				.setNome(cliente.getNome())
	    				.setCpf(cliente.getCpf())
	    				.setDataNascimento(cliente.getDataNascimento())
	    				.build();
	  }

	  private static Cliente toEntity(ClienteUpdateDto dto) {
		  return Cliente.builder().
				  	setId(dto.getId())
				  	.setNome(dto.getNome())
				  	.setCpf(dto.getCpf())
    				.setDataNascimento(dto.getDataNascimento())
				  	.build();
	  }
	  

	}
