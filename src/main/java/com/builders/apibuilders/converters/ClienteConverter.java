package com.builders.apibuilders.converters;

import org.springframework.stereotype.Component;

import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.models.dto.ClienteDto;

@Component
public class ClienteConverter extends Converter<ClienteDto, Cliente> {

	  public ClienteConverter() {
	    super(ClienteConverter::toEntity, ClienteConverter::toDto);
	  }
	  
	  private static ClienteDto toDto(Cliente cliente) {
	    return ClienteDto.builder()
	    				.setId(cliente.getId())
	    				.setNome(cliente.getNome())
	    				.setCpf(cliente.getCpf())
	    				.setDataNascimento(cliente.getDataNascimento())
	    				.build();
	  }

	  private static Cliente toEntity(ClienteDto dto) {
		  return Cliente.builder().
				  	setId(dto.getId())
				  	.setNome(dto.getNome())
				  	.setCpf(dto.getCpf())
    				.setDataNascimento(dto.getDataNascimento())
				  	.build();
	  }
	  

	}
