package com.builders.apibuilders.controllers;


import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.builders.apibuilders.converters.ClienteConverter;
import com.builders.apibuilders.converters.ClienteUpdateConverter;
import com.builders.apibuilders.domains.Cliente;
import com.builders.apibuilders.handler.RequestParameter;
import com.builders.apibuilders.models.dto.ClienteDto;
import com.builders.apibuilders.models.dto.ClienteUpdateDto;
import com.builders.apibuilders.services.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteService service;
	
	@Autowired
	private ClienteConverter converter;
	
	@Autowired
	private ClienteUpdateConverter converterUpdate;
		
	@PostMapping
	public ResponseEntity<Void> create(@Valid @RequestBody ClienteDto dto) {
		
		Cliente cliente = converter.fromDto(dto);
		cliente = service.create(cliente);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(cliente.getId())
					.toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@GetMapping
	public ResponseEntity<Page<ClienteDto>> findAll(
			@RequestParam(value = "cpf",defaultValue = "") String cpf, 
			@RequestParam(value = "page",defaultValue = "0") Integer page, 
			@RequestParam(value = "linesPerPage",defaultValue = "24") Integer linesPerPages, 
			@RequestParam(value = "orderBy",defaultValue = "nome") String orderBy, 
			@RequestParam(value = "direction",defaultValue = "ASC") String direction) {
		
		
		Cliente cliente = Cliente.builder().build();
		
		if(!cpf.isEmpty()) cliente.setCpf(cpf);
		
		PageRequest pageRequest = PageRequest.of(page,linesPerPages,Direction.valueOf(direction),orderBy);
		
		RequestParameter<Cliente> requestParameter = new RequestParameter<>(pageRequest, cliente);
		
		Page<Cliente> listClientes = service.findPage(requestParameter);
		
		Page<ClienteDto> listClientesDto = listClientes.map(item -> converter.fromEntity(item));	
		return ResponseEntity.ok().body(listClientesDto);
	}	
	
	@GetMapping(value = "/{id}" )
	public ResponseEntity<ClienteDto> find(@PathVariable Integer id) {
		
		Cliente Cliente = service.findById(id);
		ClienteDto ClienteDto = converter.fromEntity(Cliente);
		return ResponseEntity.ok().body(ClienteDto);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> update (@Valid @RequestBody ClienteDto ClienteDto, @PathVariable Integer id) {
		
		ClienteDto.setId(id);
		Cliente Cliente = converter.fromDto(ClienteDto); 
		service.update(Cliente);
		return ResponseEntity.noContent().build();
	}
	
	@PatchMapping(value = "/{id}")
	public ResponseEntity<Void> patch (@Valid @RequestBody ClienteUpdateDto ClienteDto, @PathVariable Integer id) {
		
		ClienteDto.setId(id);
		Cliente Cliente = converterUpdate.fromDto(ClienteDto); 
		service.patch(Cliente);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete (@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}
