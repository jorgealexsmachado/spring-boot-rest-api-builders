package com.builders.apibuilders.models.dto;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "set")
public class ClienteDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@NotNull
	@NotEmpty(message = "O campo nome deve ser preenchido")
	@Length(min = 3, max = 50,message = "O tamanho do campo nome deve ser entre 3 e 50 caracteres")
	private String nome;
	
	@NotNull
	@NotEmpty(message = "O campo cpf deve ser preenchido")
	@Length(min = 11, max = 11,message = "O tamanho do campo cpf deve ser de 11 caracteres")
	private String cpf;
	
	@NotNull
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dataNascimento;

	public int getIdade() {
		
		if(this.getDataNascimento()==null)
			return 0;
		
		final LocalDate dataAtual = LocalDate.now();
		final LocalDate dataNascimento = Instant.ofEpochMilli(this.getDataNascimento().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	    final Period periodo = Period.between(dataNascimento, dataAtual);
	    return  periodo.getYears();
	}
	
}
