package com.builders.apibuilders.models.dto;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "set")
public class ClienteUpdateDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@Length(min = 3, max = 50,message = "O tamanho do campo nome deve ser entre 3 e 50 caracteres")
	private String nome;
	
	@Length(min = 11, max = 11,message = "O tamanho do campo cpf deve ser de 11 caracteres")
	private String cpf;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dataNascimento;
	
}
